name 'htop'
description 'Install htop on Ubuntu Machine'
license 'Apache v2.0'

supports 'ubuntu', '>= 12.04'

depends 'apt'