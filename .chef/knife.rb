log_level                :auto
current_dir = File.dirname(__FILE__)
node_name                "provisioner"
client_key               "#{current_dir}/dummy.pem"
validation_client_name   "validator"
cookbook_path            ["#{current_dir}/../cookbooks"]
